﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment.week1
{
    public class Person
    {
        public Person(string firstName, string lastName, int tlf)
        {
            FirstName = firstName;
            LastName = lastName;
            Tlf = tlf;
        }

        public Person(string firstName, string lastName, int tlf, double bmi)
        {
            FirstName = firstName;
            LastName = lastName;
            Tlf = tlf;
            BMI = bmi;
        }

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Tlf { get; set; }
        public double BMI { get; set; }
    }
}

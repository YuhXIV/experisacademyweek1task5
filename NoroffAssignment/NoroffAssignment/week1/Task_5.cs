﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NoroffAssignment.week1
{
    public class Task_5
    {
        /// <summary>
        /// The main function of the class that run all the console prompt and methods to do the namesearch
        /// </summary>
        public void NameSearch()
        {
            List<Person> personList = new List<Person>();
            personList.Add(new Person("Kjetil", "Hel", 11111111));
            personList.Add(new Person("Orjan", "Tilde", 22222222));
            personList.Add(new Person("Markus", "Enebarn", 33333333));
            personList.Add(new Person("Emil", "Bonnerberg", 44444444));
            personList.Add(new Person("Odd Olav Hansen", "Bergedal Rikke", 55555555));
            string input;
            Console.WriteLine("Enter the name you want to search: ");

            //console prompt
            do
            {
                input = Console.ReadLine();
                if (!Letter(input)) Console.WriteLine("Only characters allowed!");

            } while (!Letter(input));

            List<string> matchingName = Search(input, personList);
            if(matchingName.Count == 0)
            {
                Console.WriteLine("Found no match");
            } else
            {
                Console.WriteLine("Matching name:");
                foreach(string c in matchingName)
                {
                    Console.Write($"{c}, ");
                }
            }
        }

        /// <summary>
        /// Compare the input of all the elements in the array and return a list of matches
        /// </summary>
        /// <param name="name">string</param>
        /// <param name="nameList">string[]</param>
        /// <returns></returns>
        List<string> Search(string name, List<Person> nameList)
        {
            List<string> matchingName = new List<string>();
            foreach (Person p in nameList)
            {
                if (p.FirstName.ToLower().Contains(name.ToLower()) || p.LastName.ToLower().Contains(name.ToLower())) // convert all to lowercase then compare
                {
                    matchingName.Add($"{p.FirstName} {p.LastName}");
                }
            }
            return matchingName;
        }

        /// <summary>
        /// Check the string is only character letters, numbers and special sign will return false.
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        bool Letter(string name) 
        {
            char[] tmp = name.ToLower().ToCharArray();
            foreach (char c in tmp)
            {
                if (!((int)c > 96 && (int)c < 123))
                {
                    return false;
                }
            }
            return true;
        }
    }
}
